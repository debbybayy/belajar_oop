<?php
    require_once ('animal.php');
    require_once ('frog.php');
    require_once ('ape.php');
        $sheep = new Animal ("Shaun");
        echo "Name : ". $sheep-> name. "<br>";
        echo "Legs : ". $sheep-> legs. "<br>";
        echo "Cold Blooded : ". $sheep-> cold_bloded. "<br> <br>";

        $kodok = new Frog ("Budug");
        echo "Name : ". $kodok-> name. "<br>";
        echo "Legs : ". $kodok-> legs. "<br>";
        echo "Cold Blooded : ". $kodok-> cold_bloded. "<br>";
        echo $kodok-> jump();

        $sungokong = new Ape ("Kera Sakti");
        echo "Name : ". $sungokong-> name. "<br>";
        echo "Legs : ". $sungokong-> legs. "<br>";
        echo "Cold Blooded : ". $sungokong-> cold_bloded. "<br>";
        echo $sungokong-> yell();

?>